package uk.co.unclealex.encoder;

import lombok.*;

@Value
@With
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Limb {

    String type;
    Position position;
}
