package uk.co.unclealex.encoder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HeadBean {

    String hairColour;
    String eyeColour;

    public static Head toHead(HeadBean headBean) {
        return new Head(headBean.getHairColour(), headBean.getEyeColour());
    }

    public static HeadBean fromHead(Head head) {
        return new HeadBean(head.getHairColour(), head.getEyeColour());
    }
}
