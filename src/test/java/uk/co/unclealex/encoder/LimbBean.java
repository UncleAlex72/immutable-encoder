package uk.co.unclealex.encoder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LimbBean {

    String type;
    Position position;

    public static Limb toLimb(LimbBean limbBean) {
        return new Limb(limbBean.getType(), limbBean.getPosition());
    }

    public static LimbBean fromLimb(Limb limb) {
        return new LimbBean(limb.getType(), limb.getPosition());
    }
}
