package uk.co.unclealex.encoder;

import org.apache.commons.io.IOUtils;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.SparkSession;
import org.codehaus.commons.compiler.ISimpleCompiler;
import org.codehaus.commons.nullanalysis.Nullable;
import org.codehaus.janino.CompilerFactory;
import org.codehaus.janino.JavaSourceClassLoader;
import org.codehaus.janino.util.resource.MapResourceCreator;
import org.codehaus.janino.util.resource.MapResourceFinder;
import org.codehaus.janino.util.resource.ResourceFinder;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class EncoderTest {

  @Test
  public void testTransformingEncoderWithNativeBeans() {
    Encoder<Person> encoder =
        Encoders.transformingEncoder(Person.class, PersonBean.class, "toPerson", "fromPerson");
    testEncoder(encoder);
  }

  @Test
  public void testTransformingEncoderWithCompiledBeans() throws IOException, ClassNotFoundException {
    ClassLoader parentClassLoader = this.getClass().getClassLoader();
    Map<String, byte[]> map = new HashMap<>();
    for (String shortClassName : new String[] {"PersonBean", "HeadBean", "LimbBean"}) {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      try (InputStream in = parentClassLoader.getResourceAsStream(shortClassName + ".txt")) {
        IOUtils.copy(in, out);
      }
      map.put("uk/co/unclealex/encoder/generated/" + shortClassName, out.toByteArray());
    }
    ResourceFinder resourceFinder = new MapResourceFinder(map);
    JavaSourceClassLoader classLoader =
        new JavaSourceClassLoader(parentClassLoader, resourceFinder, null);
    Class<?> personBeanClass = classLoader.loadClass("uk.co.unclealex.encoder.generated.PersonBean");
        Encoder < Person > encoder =
            Encoders.transformingEncoder(Person.class, personBeanClass, "toPerson", "fromPerson");
    testEncoder(encoder);
  }

  private void testEncoder(Encoder<Person> encoder) {
    SparkSession session = SparkSession.builder().master("local").appName("Freddie").getOrCreate();
    Person freddie =
        Person.builder()
            .name("Freddie")
            .age(46)
            .head(Head.builder().eyeColour("brown").hairColour("black").build())
            .limbs(
                Collections.singletonList(
                    Limb.builder().position(Position.LEFT).type("arm").build()))
            .build();
    Dataset<Person> ds1 = session.createDataset(Collections.singletonList(freddie), encoder);
    Dataset<Person> ds2 =
        ds1.map(
            (MapFunction<Person, Person>) person -> person.withAge(person.getAge() + 1), encoder);
    List<Person> people = ds2.collectAsList();
    Person expectedFreddie =
        Person.builder()
            .name("Freddie")
            .age(47)
            .head(Head.builder().eyeColour("brown").hairColour("black").build())
            .limbs(
                Collections.singletonList(
                    Limb.builder().position(Position.LEFT).type("arm").build()))
            .build();
    assertThat(people).containsExactly(expectedFreddie);
  }
}
