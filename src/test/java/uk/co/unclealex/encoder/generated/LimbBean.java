package uk.co.unclealex.encoder.generated;

import uk.co.unclealex.encoder.Limb;
import uk.co.unclealex.encoder.Position;

import java.util.Objects;

public class LimbBean {

    private String type;
    private Position position;

    public static Limb toLimb(LimbBean limbBean) {
        return new Limb(limbBean.getType(), limbBean.getPosition());
    }

    public static LimbBean fromLimb(Limb limb) {
        return new LimbBean(limb.getType(), limb.getPosition());
    }

    public LimbBean() {
        // Default no-args constructor
    }

    public LimbBean(String type, Position position) {
        this.type = type;
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LimbBean limbBean = (LimbBean) o;
        return Objects.equals(type, limbBean.type) &&
                position == limbBean.position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, position);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
