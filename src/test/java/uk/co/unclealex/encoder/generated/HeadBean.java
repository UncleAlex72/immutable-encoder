package uk.co.unclealex.encoder.generated;

import uk.co.unclealex.encoder.Head;

import java.util.Objects;

public class HeadBean {

    private String hairColour;
    private String eyeColour;

    public static Head toHead(HeadBean headBean) {
        return new Head(headBean.getHairColour(), headBean.getEyeColour());
    }

    public static HeadBean fromHead(Head head) {
        return new HeadBean(head.getHairColour(), head.getEyeColour());
    }

    public HeadBean() {
        // Default no-args constructor
    }

    public HeadBean(String hairColour, String eyeColour) {
        this.hairColour = hairColour;
        this.eyeColour = eyeColour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeadBean headBean = (HeadBean) o;
        return Objects.equals(hairColour, headBean.hairColour) &&
                Objects.equals(eyeColour, headBean.eyeColour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hairColour, eyeColour);
    }

    public String getHairColour() {
        return hairColour;
    }

    public void setHairColour(String hairColour) {
        this.hairColour = hairColour;
    }

    public String getEyeColour() {
        return eyeColour;
    }

    public void setEyeColour(String eyeColour) {
        this.eyeColour = eyeColour;
    }
}
