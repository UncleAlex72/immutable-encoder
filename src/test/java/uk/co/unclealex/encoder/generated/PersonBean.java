package uk.co.unclealex.encoder.generated;

import uk.co.unclealex.encoder.Person;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PersonBean {

    int age;
    String name;
    HeadBean head;
    List<LimbBean> limbs;

    public static Person toPerson(PersonBean personBean) {
        return new Person(
                personBean.getAge(),
                personBean.getName(),
                HeadBean.toHead(personBean.getHead()),
                personBean.getLimbs().stream().map(LimbBean::toLimb).collect(Collectors.toList()));
    }

    public static PersonBean fromPerson(Person person) {
        return new PersonBean(person.getAge(), person.getName(), HeadBean.fromHead(person.getHead()),
                person.getLimbs().stream().map(LimbBean::fromLimb).collect(Collectors.toList()));
    }

    public PersonBean() {
        // Default no-args constructor
    }

    public PersonBean(int age, String name, HeadBean head, List<LimbBean> limbs) {
        this.age = age;
        this.name = name;
        this.head = head;
        this.limbs = limbs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonBean that = (PersonBean) o;
        return age == that.age &&
                Objects.equals(name, that.name) &&
                Objects.equals(head, that.head) &&
                Objects.equals(limbs, that.limbs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name, head, limbs);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HeadBean getHead() {
        return head;
    }

    public void setHead(HeadBean head) {
        this.head = head;
    }

    public List<LimbBean> getLimbs() {
        return limbs;
    }

    public void setLimbs(List<LimbBean> limbs) {
        this.limbs = limbs;
    }
}
