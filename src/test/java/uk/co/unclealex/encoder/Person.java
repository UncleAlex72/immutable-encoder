package uk.co.unclealex.encoder;

import lombok.*;

import java.util.List;

@Value
@With
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Person {

    int age;
    String name;
    Head head;
    List<Limb> limbs;
}
