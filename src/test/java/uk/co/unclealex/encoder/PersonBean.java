package uk.co.unclealex.encoder;

import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@With
public class PersonBean {

    int age;
    String name;
    HeadBean head;
    List<LimbBean> limbs;

    public static Person toPerson(PersonBean personBean) {
        return new Person(
                personBean.getAge(),
                personBean.getName(),
                HeadBean.toHead(personBean.getHead()),
                personBean.getLimbs().stream().map(LimbBean::toLimb).collect(Collectors.toList()));
    }

    public static PersonBean fromPerson(Person person) {
        return new PersonBean(person.getAge(), person.getName(), HeadBean.fromHead(person.getHead()),
                person.getLimbs().stream().map(LimbBean::fromLimb).collect(Collectors.toList()));
    }

}
