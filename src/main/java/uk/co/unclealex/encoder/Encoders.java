package uk.co.unclealex.encoder;

import org.apache.spark.sql.catalyst.JavaTypeInference;
import org.apache.spark.sql.catalyst.JavaTypeInference$;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.expressions.*;
import org.apache.spark.sql.catalyst.expressions.objects.AssertNotNull;
import org.apache.spark.sql.catalyst.expressions.objects.StaticInvoke;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.ObjectType;
import org.apache.spark.sql.types.StructType;
import org.spark_project.guava.reflect.TypeToken;
import scala.collection.Seq;
import scala.reflect.ClassTag$;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import static scala.collection.JavaConverters.collectionAsScalaIterableConverter;

public class Encoders {

  private Encoders() {
    // This class cannot be instantiated.
  }

  private static final Method serializeForMethod;

  static {
    try {
      //noinspection JavaReflectionMemberAccess
      serializeForMethod =
          JavaTypeInference$.class.getMethod(
              "org$apache$spark$sql$catalyst$JavaTypeInference$$serializerFor",
              Expression.class,
              TypeToken.class);
    } catch (NoSuchMethodException e) {
      throw new IllegalStateException("Cannot find private serializer method", e);
    }
    serializeForMethod.setAccessible(true);
  }

  private static <BEAN> Expression serializerFor(
      Class<BEAN> beanClass, Expression transformedInputObject) {
    try {
      return (Expression)
          serializeForMethod.invoke(
              JavaTypeInference$.MODULE$, transformedInputObject, TypeToken.of(beanClass));
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new IllegalStateException(
          "Cannot call the private serializeFor method for class " + beanClass, e);
    }
  }

  public static <BEAN, IMMUTABLE> ExpressionEncoder<IMMUTABLE> transformingEncoder(
      Class<IMMUTABLE> immutableClass,
      Class<BEAN> beanClass,
      String toFunctionName,
      String fromFunctionName) {

    DataType schema = JavaTypeInference.inferDataType(beanClass)._1();
    assert (schema instanceof StructType);

    CreateNamedStruct serializer = serializerFor(immutableClass, beanClass, fromFunctionName);
    Expression deserializer =
        new StaticInvoke(
            beanClass,
            new ObjectType(immutableClass),
            toFunctionName,
            asSeq(JavaTypeInference.deserializerFor(beanClass)),
            true,
            true);

    return new ExpressionEncoder<IMMUTABLE>(
        (StructType) schema,
        false,
        (Seq) serializer.flatten(),
        deserializer,
        ClassTag$.MODULE$.apply(immutableClass));
  }

  private static <BEAN, IMMUTABLE> CreateNamedStruct serializerFor(
      Class<IMMUTABLE> immutableClass, Class<BEAN> beanClass, String fromMethodName) {
    Expression inputObject = new BoundReference(0, new ObjectType(immutableClass), true);
    Expression nullSafeInput = new AssertNotNull(inputObject, asSeq("top level input bean"));
    Expression transformedInputObject =
        new StaticInvoke(
            beanClass, new ObjectType(beanClass), fromMethodName, asSeq(nullSafeInput), true, true);
    Expression serializer = serializerFor(beanClass, transformedInputObject);
    if (serializer instanceof If && ((If) serializer).falseValue() instanceof CreateNamedStruct) {
      return (CreateNamedStruct) ((If) serializer).falseValue();
    } else {
      Expression value = Literal$.MODULE$.apply("value");
      return new CreateNamedStruct(asSeq(value, serializer));
    }
  }

  private static <T> Seq<T> asSeq(T... elements) {
    return collectionAsScalaIterableConverter(Arrays.asList(elements)).asScala().toSeq();
  }
}
